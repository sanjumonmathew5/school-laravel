<?php

namespace App\Http\Controllers;

use App\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request){
        $http = new \GuzzleHttp\Client;
        try{
            $response = $http->post('http://school-management-system.test/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => 2,
                    'client_secret' => 'rXtso1z9Snczmau5788dLzxW8Tu1w9zuFSf9R0I2',
                    'username' => $request->username,
                    'password' => $request->password,
                ]
            ]);
            return $response->getBody();
        } catch(\GuzzleHttp\Exception\BadResponseException $e) {
            if($e->getCode() === 400) {
                return response()->json('Invalid request please enter a username or password.', $e->getCode());
            } else if($e->getCode() === 401){
                return response()->json('Your credentials are incorrect please try again.', $e->getCode());
            } else {
                return response()->json('Something went wrong on the server.', $e->getCode());
            }
        }  
    }
    public function register(Request $request){

        $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        $validatedData['password'] = Hash::make($request->password);
        $user = User::create($validatedData);
        return response(['user'=> $user]);

        // return User::create([
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'password' => Hash::make($request->password),
        // ]);
    }

    public function logout(){
        auth()->user()->tokens->each(function($token, $key){
            $token->delete();
        });
        return response()->json('Logged out successfully', 200);
    }
}
